import serial, time

windows = 'COM3'
linux = '/dev/ttyUSB0'

def getOsc(input):
    # type: (object) -> object
    try:
        port = serial.Serial('COM14', baudrate=115200, timeout=.25)
        while True:
            port.write(input)
            resposta = port.readline()
            if resposta == '':
               # port.close()
                return('TIMEOUTsplit')
            else:
                return (resposta)
    except Exception as e:
        print("Erro' %s" % e)

def getHx(input):
    try:
        port = serial.Serial('COM14', baudrate=115200, timeout=1)
        while True:
            port.write(input)
            resposta = port.read(64)
            if resposta == '':
                return('0BTIMEOUT## PESO')
            else:
                return (resposta)
    except Exception as e:
        print("Erro' %s" % e)

def getGps(input):
    # type: (object) -> object
    try:
        port = serial.Serial('COM14', baudrate=115200, timeout=2)
        while True:
            port.write(input)
            resposta = port.readline()
            if resposta == '':
               # port.close()
                return('TIMEOUTsplit')
            else:
                return (resposta)
    except Exception as e:
        print("Erro' %s" % e)

class gps:

    @staticmethod
    def read():
        receiveGPS = getGps('07GPSOK')
        #string = receiveGPS.split('\r\n')
        gpsStr = receiveGPS.find('$GPGGA')
        gpsStr2 = receiveGPS.find('$GPGSA')
        gps = receiveGPS[gpsStr:gpsStr2]

        return gps

    @staticmethod
    def array():
        a = gps.read()
        array = a.split(',')
        return array

    @staticmethod
    def utc():
        a = gps.read()
        utc = a.split(',')
        return utc[1]

    @staticmethod
    def lat():
        a = gps.read()
        lat = a.split(',')
        return lat[2] + lat[3]

    @staticmethod
    def log():
        a = gps.read()
        log = a.split(',')
        return log[4] + log[5]


    @staticmethod
    def quality():
        a = gps.read()
        qual = a.split(',')
        return qual[6]

    @staticmethod
    def nsat():
        a = gps.read()
        nsat = a.split(',')
        return nsat[7]

    @staticmethod
    def alt():
        a = gps.read()
        alt = a.split(',')
        return alt[9]

    @staticmethod
    def check():
        a = gps.read()
        check = a.split(',')

        return check[14]



if __name__ == '__main__':
    while True:
        receiveGPS = getOsc("07FREQV")
        print receiveGPS

